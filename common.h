#ifndef COMMON_H

#define COMMON_H
#include "types.h"
#include "const.h"
#include "ip.h"
#include "arp.h"
#include "ether.h"

#define LINKER_DEF

#define hexchar(x) (x >= '0' && x <= '9') ? \
                         (x - '0') : (x - 'a' + 10)

#define decchar(x) (x - '0')

extern unsigned char buf[];
extern int sock_fd;
#endif

