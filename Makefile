cc = gcc -Werror -m32

all: linker

server: main.o ether.o arp.o ip.o timer.o server.o debug.o
	$(cc) main.o ether.o arp.o ip.o server.o timer.o	\
		debug.o	\
		-o server
		
linker: main.o ether.o arp.o ip.o timer.o linker.o debug.o
	$(cc) main.o ether.o arp.o ip.o timer.o linker.o 	\
		debug.o	\
		-o linker

main.o: main.c
	$(cc) -c main.c

ether.o: ether.c
	$(cc) -c ether.c

ip.o: ip.c
	$(cc) -c ip.c

arp.o: arp.c
	$(cc) -c arp.c

vpn.o: vpn.c
	$(cc) -c vpn.c

timer.o: timer.c
	$(cc) -c timer.c

server.o: server.c
	$(cc) -c server.c

linker.o: linker.c
	$(cc) -c linker.c

debug.o: debug.c
	$(cc) -c debug.c


clean:
	rm -f *o server linker

run:
	sudo ./linker 2> /dev/null

runserver:
	sudo ./server 2> /dev/null
