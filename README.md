This project is a link aggregation program



NAT problem:
        alloc a new port number for a triplet (source, dest, s_port)
        1. if a triplet has been assigned a port, we want to find the port number quickly
        2. if has not, we want to find a free port number quickly

        HASH vs. BST:
        1. when the traffic is low, hash works better,
           however when the traffic is high, BST works better
        2. we choose hash
        3. a combined solution also exists, yet such a trouble to implement

