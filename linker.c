/*
 *      Only Consider TCP / UDP
 */

#include <linux/in.h>
#include <linux/ip.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include "common.h"


static char NAT_server[4];

#define PORT_NULL       (uint16)65536

#define LOCAL_PORT_START      34768   /* Defined in /proc/sys/net/ipv4/ip_local_range */
#define LOCAL_PORT_SIZE       26233
struct port_t {
        int key;
        struct port_t *next;
        uint16 port_num;
}port_pool[LOCAL_PORT_SIZE];
struct port_t *port_head;


#define HASH_INUSE      1
#define HASH_EMPTY      0
#define HASH_OCCUP      2
struct nhash_item {
        int saddr, daddr;
        uint16 port;
        uint16 ttl;
        struct port_t *port_entry;
        char used;
};
#define NAT_HASH_SIZE   28233 * 2
static struct nhash_item nhash[NAT_HASH_SIZE];

struct r_nhash_item {
        int key;
        struct r_nhash_item *next;
}
static head = {0, null};


void nat_init()
{
        int i;
        for (i = 0; i < LOCAL_PORT_SIZE; i ++) {
                port_pool[i].port_num = i + LOCAL_PORT_START;
                port_pool[i].key = NAT_HASH_SIZE;
                if (i != LOCAL_PORT_SIZE)
                        port_pool[i].next = &port_pool[i+1];
                else
                        port_pool[i].next = null;
        }
        port_head = &port_pool[0];

        NAT_server[0] = 114;
        NAT_server[1] = 212;
        NAT_server[2] = 97;
        NAT_server[3] = 87;
        
}

static inline struct port_t* port_alloc()
{
        struct port_t* ret;
        ret = port_head;
        port_head = port_head->next;
        return ret;
}

static inline void port_release(struct port_t* port)
{
        port->next = port_head;
        port_head = port;
}


static inline bool hash_reverse_add(int key)
{
        struct r_nhash_item *entry = (struct r_nhash_item*) malloc (sizeof(struct r_nhash_item));

        if (entry == null) {
                return false;
                printf("can't alloc\n");
        }
        entry->key = key;

        entry->next = head.next;
        head.next = entry;

        return true;
}


/* Attention, i didn't change the port to the Machine Endding Format */
int get_hash_key(int val1, int val2, uint16 val3)
{
        int key, key_cand = -1, cnt = 0;

        key = (val1 * 3671 + val2 * 719 + val3 * 137) % NAT_HASH_SIZE; 
        if (key < 0) 
                key += NAT_HASH_SIZE;

        for (; nhash[key].used; key = (key + 1) % LOCAL_PORT_SIZE, cnt ++) {
                if (ip_cmp((ip_ptr)&val1, (ip_ptr)&nhash[key].saddr) 
                     && ip_cmp((ip_ptr)&val2, (ip_ptr)&nhash[key].daddr)
                     && (nhash[key].port = val3)) {
                        assert(nhash[key].used == HASH_INUSE);
                        break;
                }

                if ((nhash[key].used == HASH_OCCUP) && (key_cand == -1))
                        key_cand = key;
        }

        if ((nhash[key].used == HASH_EMPTY) && (key_cand != -1))
                return key_cand;
        else
                return key;
}

inline static void hash_alloc(int key, int saddr, int daddr, uint16 port)
{
        nhash[key].saddr = saddr;
        nhash[key].daddr = daddr;
        nhash[key].port = port;
        nhash[key].ttl = 0;
        nhash[key].used = HASH_INUSE;
}

inline static void hash_release(int key)
{
        nhash[key].saddr = 0;
        nhash[key].daddr = 0;
        nhash[key].port = 0;
        nhash[key].used = HASH_OCCUP;
}

/* 
 * Attention, nat_assign() would change the content of saddr, and port
 */
bool nat_assign(ip_ptr saddr, ip_ptr daddr, uint16 *port)
{
        int key;
        key = get_hash_key(*(int*)saddr, *(int*)daddr, *port);

        if (nhash[key].used != HASH_INUSE) {

                /* No available port */
                if (port_head == NULL)
                        return false;

                /* can't add hash entry in use to the links */
                if (!hash_reverse_add(key))
                        return false;

                hash_alloc(key, *(int*)saddr, *(int*)daddr, *port);
                nhash[key].port_entry = port_alloc();
                nhash[key].port_entry->key = key;
        }

        memcpy(saddr, (ip_ptr)&ip_home.saddr, IP_ALEN);
        *port = htons(nhash[key].port_entry->port_num);
        printf("port&key&key: %d %d %d\n", htons(*port), key,
                        port_pool[htons(*port) - LOCAL_PORT_START].key);
        return true;
}

bool nat_reverse(int* daddr, uint16 *port)
{
        int key = port_pool[htons(*port) - LOCAL_PORT_START].key;

        if ((key == NAT_HASH_SIZE) || (nhash[key].used != HASH_INUSE))
                return false;

        *daddr = nhash[key].saddr;
        if (nhash[key].port != PORT_NULL)
                *port = nhash[key].port;

        return true;
}


void tcp_checksum(pac_ptr packet)
{
        struct iphdr *iph = (struct iphdr*)packet;
        struct psuedo {
                int saddr, daddr;
                char zero, proto; 
                unsigned short len;
        }psh;
        unsigned long ans = 0;
        uint16 *p;
        int i, ptr;
        //uint16 ret;
        uint16 len = htons(iph->tot_len) - iph->ihl * 4;

        psh.saddr = iph->saddr;
        psh.daddr = iph->daddr;
        psh.zero = 0;
        psh.proto = iph->protocol;
        psh.len = htons(len);

        if (iph->protocol != IPPROTO_UDP)
                ptr = 8;
        else
                ptr = 3;
        
        p = (uint16*)&psh;
        for (i = 0; i < 6; i ++)
                ans += p[i];

        p = (uint16*)(&packet[iph->ihl * 4]);
        //ret = p[ptr];
        p[ptr]= 0;
        for (i = 0; i < len / 2; i ++)
                ans += p[i];

        while (ans >> 16)
                ans = (ans & 0xffff) + (ans >> 16);

        p[ptr] = (unsigned short)~ans;
}

void nat_maintain()
{
        struct r_nhash_item *it = head.next, *pre = &head;
        int key;
        while (it != null) {
                key = it->key;
                nhash[key].ttl ++;

                if (nhash[key].ttl < LINK_TIME_LIMIT)
                        pre = pre->next;
                else {
                        pre->next = it->next;
                        port_release(nhash[key].port_entry);
                        hash_release(key);
                        free(it);
                }

                it = pre->next;
        }
}

void ip_unpack_forward(pac_ptr packet)
{
        struct iphdr *iph = (struct iphdr*)packet;
        uint16 *port = (unsigned short*)&packet[iph->ihl*4];
        bool flag;
        
        switch (iph->protocol) {
                case IPPROTO_TCP:
                case IPPROTO_UDP:
                        flag = nat_assign((ip_ptr)&iph->saddr, (ip_ptr)&iph->daddr, port);     
                        tcp_checksum(packet);
                        break;

                default:
                        printf("can't support\n");
                        break;
        }

        if (flag) {
                printf(" Daddr: ");
                ip_println((ip_ptr)&iph->daddr);
                ip_forward(packet);
        }
}


static uint16 ip_id = 0x2000;
void ip_trans_forward(pac_ptr packet)
{
        struct iphdr *iph = (struct iphdr*)packet;
        uint16 *port = (uint16*)&packet[iph->ihl*4];
        bool flag;
        uint16 len;

        port++;
        switch (iph->protocol) {
                case IPPROTO_TCP:
                case IPPROTO_UDP:
                        flag = nat_reverse(&iph->daddr, port);

                        ip_setcheck(iph);
                        tcp_checksum(packet);
                        break;

                default:
                        flag = nat_reverse(&iph->daddr, PORT_NULL);
                        break;
        }

        /* pack to server */
        if (flag) {
                memcpy(&ip_home.daddr, NAT_server, IP_ALEN);
                len = htons(iph->tot_len);
                ip_home.tot_len = htons(len + IP_HLEN);
                ip_home.ttl = iph->ttl;
                ip_home.id = htons(ip_id ++);
                ip_home.tos = iph->tos;
                ip_setcheck(&ip_home);

                memcpy(ip_buf, &ip_home, IP_HLEN);
                memcpy(&ip_buf[IP_HLEN], packet, len);

                ip_forward(ip_buf);
        }
}
