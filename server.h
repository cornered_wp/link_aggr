#ifndef _LINKER_H_
#define _LINKER_H_

#include "types.h"
#include <linux/ip.h>

extern uint32 linkers[];

int linker_init();
const ip_ptr linker_alloc(struct iphdr*);

#endif
