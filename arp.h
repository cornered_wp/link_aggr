#ifndef __ARP_H__
#define __ARP_H__
#include "types.h"

struct arphdr2 {
	short ar_hrd;		/* format of hardware address	*/
	short ar_pro;		/* format of protocol address	*/
	unsigned char	ar_hln;		/* length of hardware address	*/
	unsigned char	ar_pln;		/* length of protocol address	*/
	short ar_op;		/* ARP opcode (command)		*/

	unsigned char		ar_sha[6];	/* sender hardware address	*/
	unsigned char		ar_sip[4];		/* sender IP address		*/
	unsigned char		ar_tha[6];	/* target hardware address	*/
	unsigned char		ar_tip[4];		/* target IP address		*/

};

void arp_handle(unsigned char*);
const mac_ptr arp_lookup(const ip_ptr);
void arp_age();

#endif
