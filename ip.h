#ifndef __IP_H__
#define __IP_H__
#include "types.h"

inline bool ip_cmp(const ip_ptr, const ip_ptr);
inline bool ip_mcmp(const ip_ptr, const ip_ptr, const ip_ptr);    // With Mask
char* ip_tostr(const ip_ptr);
inline void ip_print(const ip_ptr);
inline void ip_println(const ip_ptr);
const ip_ptr ip_getip();
void ip_forward(pac_ptr);

extern struct iphdr ip_home;
extern unsigned char ip_buf[];

#endif
