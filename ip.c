#include <linux/ip.h>
#include <linux/if_ether.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

#ifndef LINKER_DEF
#include "server.h"
#else
#include "linker.h"
#endif

static unsigned char CAMPUS_IP[] = {114, 212, 0, 0};
static unsigned char CAMPUS_MASK[] = {255,255,0,0};
static unsigned char GATEWAY[] = {114, 212, 96, 1};


unsigned char ip_buf[MAX_BUF_SIZE];
struct iphdr ip_home;


inline void ip_println(const ip_ptr ip)
{
        printf("%d.%d.%d.%d\n", ip[0], ip[1], ip[2], ip[3]);
}
inline void ip_print(const ip_ptr ip)
{
        printf("%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
}


bool ip_fromStr(ip_ptr ip, unsigned char* str)
{
        int i, k;
        for (i = 0; i < IP_ALEN; i ++) {
                ip[i] = 0;
                while (*str != '.' && *str != '\n') {
                        k = *str - '0';
                        if (k < 0 || k > 9) {
                                fprintf(stderr, "Error: Invalid IP address\n");
                                return false; 
                        }
                                
                        ip[i] = ip[i] * 10 + k;
                        str ++;
                }
                *str ++;
        }
        return true;
}

void ip_init(unsigned char *str)
{

        if (!ip_fromStr((ip_ptr)&ip_home.saddr, str)) {
                fprintf(stderr, "Error: .config format wrong @ ip");
                exit(1);
        }

        printf("IP Address Intialized: ");
        ip_println((ip_ptr)&ip_home.saddr);
        ip_home.version = 4;
        ip_home.ihl = 5;
        ip_home.protocol = IP_VPN_PROTO;
        ip_home.frag_off = 0;
}

/** You must be careful, because, ip_str() use a inner buffer
 *  which means you cant use this in a printf to print two ips
 */
char* ip_tostr(const ip_ptr ip)
{
        static char str[16];
        int len = 14, i, j;
        str[15] = '\0';
        for (i = 3; i >= 0; i --) {
                j = ip[i];
                while (j > 0) {
                        str[len--] = j % 10 + '0';
                        j = j / 10;
                }
                str[len --] = '.';
        }

        return &str[len + 2];
}

inline bool ip_meq(const ip_ptr ip1, const ip_ptr ip2, const ip_ptr mask)
{
        return ((*(int*)ip1 & *(int*)mask) == (*(int*)ip2 & *(int*)mask));
}

inline bool ip_cmp(const ip_ptr ip1, const ip_ptr ip2)
{
        int i;
        for (i = 0; i < 4; i ++)
                if (ip1[i] != ip2[i])
                        return false;

        return true;
}

bool ip_checksum(struct iphdr* ip_head)
{
        unsigned short *p;
        unsigned int ans = 0;
        int i, len;
        len = ip_head->ihl * 4;

        p = (unsigned short*)ip_head;
        for (i = 0; i < len / 2; i++, p++)
                ans += htons(*p);
        while (ans >> 16)
                ans = (ans & 0xffff) + (ans >> 16);

        if (ans == 0xffff)
                return true;
        else
                return false;
}

void ip_setcheck(struct iphdr* ip_head)
{
        unsigned short *p;
        unsigned int ans = 0;
        int i, len;
        len = ip_head->ihl * 4;

        p = (unsigned short*)ip_head;
        ip_head->check = 0;
        for (i = 0; i < len / 2; i++, p++)
                ans += htons(*p);
        while (ans >> 16)
                ans = (ans & 0xffff) + (ans >> 16);

        ip_head->check = htons(~ans);   
}

void ip_forward(pac_ptr packet)
{
        struct iphdr* iph = (struct iphdr*)packet;
        ip_ptr tip = (ip_ptr)&iph->daddr;
        const mac_ptr mac_addr;

        ip_setcheck(iph);


        if (ip_meq(tip, CAMPUS_IP, CAMPUS_MASK))
                mac_addr = arp_lookup(tip);
        else
                mac_addr = arp_lookup(GATEWAY);

        if (mac_addr == null) {
                arp_request(tip);
                arp_cache_in(packet);
        }
        else
                mac_send(mac_addr, packet, htons(iph->tot_len), ETH_P_IP);

}

bool link_customer(ip_ptr ip)
{
        return true;
}

static bool outer(const ip_ptr ip)
{
        if (ip_meq(ip, CAMPUS_IP, CAMPUS_MASK))
                return false;
        return true;
}

void ip_handle(pac_ptr packet)
{
        struct iphdr* iph = (struct iphdr*)packet;
        pac_ptr newpack;

        if (!ip_checksum(iph)) 
                return ;

#ifdef LINKER_DEF
        if (iph->protocol == IP_VPN_PROTO) {
                ip_unpack_forward(&packet[IP_HLEN]);
        }
        else if (outer((ip_ptr)&iph->saddr) && ip_cmp((ip_ptr)&ip_home.saddr, (ip_ptr)&iph->daddr)) {
                ip_trans_forward(packet);
        }
#else
        if (link_customer((ip_ptr)&iph->saddr) && outer((ip_ptr)&iph->daddr))  {
                ip_pack_forward(packet);
        }

#endif
}


const ip_ptr ip_getip()
{
        return (ip_ptr)&ip_home.saddr;
}
