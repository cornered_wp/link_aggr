#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <sys/socket.h>
#include <unistd.h>

#include "common.h"


static struct ethhdr eth_home;
static const unsigned char MAC_BROADCAST[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
static unsigned char mac_buf[MAX_BUF_SIZE];

static struct sockaddr_ll dif;

inline void mac_println(mac_ptr mac)
{
        printf("%02x.%02x.%02x.%02x.%02x.%02x\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

inline void mac_print(mac_ptr mac)
{
        int i;
        for (i = 0; i < 6; i ++)
                printf("%02x.", mac[i]);
}

bool mac_eq(mac_ptr mac1, mac_ptr mac2)
{
        int i;
        for (i = 0; i < ETH_ALEN; i ++)
                if (mac1[i] != mac2[i])
                        return false;
        return true;
}

bool mac_init(unsigned char* mac_str)
{
        int i, x1, x2;
        char d1, d2;

        for (i = 0; i < ETH_ALEN; i ++) {
                d1 = mac_str[i * 3];
                d2 = mac_str[i * 3 + 1];
                x1 = hexchar(d1);
                x2 = hexchar(d2);
                eth_home.h_source[i] = x1 * 16 + x2;

                if ((x1 > 15) || (x2 > 15))
                        return false;
                if ((x1 < 0) || (x2 < 0))
                        return false;
        }


        memset(&dif, 0, sizeof(dif));
        dif.sll_family = AF_PACKET;
        dif.sll_ifindex = if_nametoindex("eth0");
        printf("ifindex: %d\n", dif.sll_ifindex);
        return true;
}


void mac_handle(pac_ptr packet, int len)
{
        struct ethhdr* head = (struct ethhdr*)packet;
        unsigned char *p;

        p = &packet[ETH_HLEN];

        if (mac_eq(head->h_source, eth_home.h_source))
                return ;

//        debug_receive_frame(packet, len);

        switch (htons(head->h_proto)) {
                case ETH_P_IP:
                        ip_handle(p);
                        break;

                case ETH_P_ARP:
                        arp_handle(p);
                        break;

                default:
                        fprintf(stderr, "Warning: Ignored a frame for unimplemented protocal\n");
                        break;
        }
}

void mac_send(const mac_ptr mac, pac_ptr packet, int len, short proto)
{
        int n_write;
        
        if (mac == null) {
                printf("mac is NULL\n");
                return ;
        }
                
        memcpy(eth_home.h_dest, mac, ETH_ALEN);
        eth_home.h_proto = htons(proto);

        if (proto == ETH_P_ARP)
                memcpy(((struct arphdr2*)packet)->ar_sha, eth_home.h_source, ETH_ALEN);

        memcpy(mac_buf, &eth_home, ETH_HLEN);
        memcpy(&mac_buf[ETH_HLEN], packet, len);

        n_write = sendto(sock_fd, mac_buf, len + ETH_HLEN, 0, (struct sockaddr*)&dif, sizeof(dif));
        if (n_write < 0)
                fprintf(stderr, "Error: can\'t send frame\n");
        else
                debug_sent_frame(mac_buf, len + ETH_HLEN);
}

inline void mac_broadcast(pac_ptr packet, int len, short proto)
{
        mac_send(MAC_BROADCAST, packet, len, proto);
}
