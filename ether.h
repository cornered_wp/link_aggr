#ifndef __ETHER_H__
#define __ETHER_H__
#include "types.h"

bool mac_init(unsigned char* buf);
inline void mac_broadcast(pac_ptr, int, short);
void mac_handle(pac_ptr, int);
void mac_send(const mac_ptr, pac_ptr, int, short);

#endif
