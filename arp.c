#include <linux/if_ether.h>
#include <net/if_arp.h>
#include <linux/ip.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "arp.h"
#include "ip.h"
#include "common.h"

#define MAX_ARP_INFO    100
#define MAX_TTL         300

struct arp_item{
	unsigned char ip_addr[4];
	unsigned char mac_addr[6];
	int ttl;
};

static struct arp_item arp_table[MAX_ARP_INFO];
static struct arphdr2 arp_req;
static int arps = 0;

static void arp_table_print()
{
        int i;
        for (i = 0; i < arps; i ++) {
                ip_println(arp_table[i].ip_addr);
                mac_println(arp_table[i].mac_addr);
        }
}

static int arp_update(mac_ptr mac, ip_ptr ip)
{
        /* Now, we dont support arp delete */
        int i, j;
	for (i = 0; i < arps; i++)
		if (ip_cmp(arp_table[i].ip_addr, ip))
                        goto __update;


	if (arps == MAX_ARP_INFO){
                i = 0;
		for (j = 1; j < arps; j++)
			if (arp_table[j].ttl >= arp_table[i].ttl)
				i = j;
	}
	else {
		i = arps;
		arps ++;
	}


__update:
	memcpy(arp_table[i].ip_addr, ip, 4);
	memcpy(arp_table[i].mac_addr, mac, 6);
	arp_table[i].ttl = 0;

	return i;
}


/**
 * Send an ARP request
 */
void arp_request(ip_ptr ip)
{
        struct arphdr2 *arp;
        int len;

        arp = (struct arphdr2*)malloc(sizeof(struct arphdr2));

       	arp->ar_hrd = htons(0x0001);
	arp->ar_pro = htons(ETH_P_IP);
	arp->ar_hln = ETH_ALEN;
	arp->ar_pln = 4;
	arp->ar_op = htons(ARPOP_REQUEST);
        memcpy(&arp->ar_sip, ip_getip(), IP_ALEN);
        memcpy(&arp->ar_tip, ip, IP_ALEN);

        mac_broadcast((pac_ptr)arp, sizeof(struct arphdr2), ETH_P_ARP);
        free(arp);
}

const mac_ptr arp_lookup(const ip_ptr ip)
{
        int i;
        for (i = 0; i < arps; i ++) {
                if (ip_cmp(arp_table[i].ip_addr, ip))
                        return arp_table[i].mac_addr;
        }

        return null;
}

#define MAX_CACHE_SIZE  50
static pac_ptr cache_q[MAX_CACHE_SIZE];
static bool cache_inuse[MAX_CACHE_SIZE];


void arp_cache_in(pac_ptr ip_packet)
{
        struct iphdr* iph = (struct iphdr*) ip_packet;
        int i;
        short len = htons(iph->tot_len);

        for (i = 0; i < MAX_CACHE_SIZE; i ++)
                if (!cache_inuse[i]) {
                        cache_q[i] = malloc(len);
                        memcpy(cache_q[i], ip_packet, len);
                        cache_inuse[i] = 1;
                        return ;
                }

        // if enters the loop, means no cache, throw the packet
}

void arp_cache_out(ip_ptr ip)
{
        struct iphdr* iph;
        int i;
        for (i = 0; i < MAX_CACHE_SIZE; i ++) 
                if (cache_inuse[i]) {
                        iph = (struct iphdr*)cache_q[i];
                        if (ip_cmp((ip_ptr)(&iph->daddr), ip)) {
                                ip_forward(cache_q[i]);
                                free(cache_q[i]);
                                cache_inuse[i] = 0;
                        }
                }
}

void arp_init()
{
	arp_req.ar_hrd = htons(0x0001);
	arp_req.ar_pro = htons(ETH_P_IP);
	arp_req.ar_hln = ETH_ALEN;
	arp_req.ar_pln = 4;
	arp_req.ar_op = htons(ARPOP_REQUEST);

        memset(cache_inuse, 0, sizeof(cache_inuse));
}

void arp_test()
{
        unsigned char ip[4];
        ip[0] = 114;
        ip[1] = 212;
        ip[2] = 96;
        ip[3] = 1;
        arp_request(ip);
        
        ip[0] = 114;
        ip[1] = 212;
        ip[2] = 97;
        ip[3] = 87;
        arp_request(ip);
/*
        ip[2] = 132;
        ip[3] = 18;
        arp_request(ip);
        */
}


void arp_handle(unsigned char *p) 
{
        struct arphdr2 *arp_head = (struct arphdr2*)p;
        int ind;

        /* I dont need to handle */
//        printf("arp handle\n");
        if (arp_head->ar_op != htons(ARPOP_REPLY))
                return ;

        if (!ip_cmp(arp_head->ar_tip, (ip_ptr)&ip_home.saddr))
                return ;

        arp_update(arp_head->ar_sha, arp_head->ar_sip);
        arp_cache_out(arp_head->ar_sip);
//        arp_table_print();
}


/** When to do the aging is a problem */
inline void arp_age()
{
        int i;
        for (i = 0; i < arps; i ++) {
                arp_table[i].ttl ++;
        }
}


