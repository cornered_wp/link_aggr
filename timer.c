#include "common.h"
#include <signal.h>

void sigroutine(int sig)
{
        static long time = 0;
        time ++;

        arp_age();

#ifdef LINKER_DEF
        nat_maintain();
#else
        linker_age();
#endif

        signal(SIGALRM, sigroutine);
        alarm(1);
}

void timer_init()
{
        signal(SIGALRM, sigroutine);
        alarm(1);
}

