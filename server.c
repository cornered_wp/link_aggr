#include <linux/ip.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common.h"


#define MAX_LINKERS    8 

struct linker_t {
        long load;
        unsigned char ip_addr[4];
};
static struct linker_t linkers[MAX_LINKERS];
static int lk_cnt;


#define MAX_HASH_SIZE   32767
#define HASH_EMPTY      0
#define HASH_INUSE      1
#define HASH_OCCUP      2
struct hash_item {
        unsigned char saddr[4], daddr[4];
        long ttl;
        char link_n, used;
};
struct rhash_item {
        int key;
        struct rhash_item *next;
}head;
static struct hash_item hash[MAX_HASH_SIZE];


int linker_init()
{
        ip_ptr p;
        int i;
        
        for (i = 0; i < MAX_LINKERS; i ++)
                memset(linkers, 0, sizeof(linkers));
        head.next = null;

        lk_cnt = 1;
        p = linkers[0].ip_addr;
        p[0] = 192;
        p[1] = 168;
        p[2] = 1;
        p[3] = 114;

        printf("Remote Linker Initialized: (");
        for (i = 0; i < lk_cnt; i ++)
                ip_print(linkers[i].ip_addr);
        printf(")\n");
}

static void hash_reverse_add(ip_ptr ip1, ip_ptr ip2, int key)
{
        struct rhash_item* p = (struct rhash_item*)malloc(sizeof(struct rhash_item));

        memcpy(&p->saddr, ip1, IP_ALEN);
        memcpy(&p->daddr, ip2, IP_ALEN);
        p->key = key;

        p->next = head.next;
        head.next = p;
}

#define LINK_ALLOC      1
#define LINK_NONAL      0
static int get_hash_key(int *ip1, int *ip2, bool alloc)
{
        long key, key_cand = -1;
        bool fflag = false;
        int cnt = 0;
        key = (*ip1 * 27449 + *ip2) % MAX_HASH_SIZE;
        
        for (; hash[key].used; key = (key + 1) % MAX_HASH_SIZE, cnt ++) {
                if (hash[key].used == HASH_OCCUP) {
                        if (key_cand == -1)
                                key_cand = key;
                }

                else if (ip_cmp((ip_ptr)ip1, hash[key].saddr) && ip_cmp((ip_ptr)ip2, hash[key].daddr)) {
                        fflag = true;
                        break;
                }

                if (cnt == MAX_HASH_SIZE * 3 / 4)
                        return -1;
        }

        if (fflag) {
                hash[key].ttl = 0;
                return key;
        }
        else if (!alloc)
                return -1;
        else {
                if (key_cand != -1)
                        key = key_cand;
                hash[key].used = HASH_INUSE;
                memcpy(hash[key].saddr, ip1, IP_ALEN);
                memcpy(hash[key].daddr, ip2, IP_ALEN);
                hash[key].ttl = 0;
                hash[key].link_n = 0xff;
                hash_reverse_add((ip_ptr)ip1, (ip_ptr)ip2, key);
                return key;
        }
}


/* check hash item periodically */
void linker_age()
{
        struct rhash_item *it = head.next, *pre = &head;
        int key, i;
        static uint16 tick = 0;

        while (it != null) {
                key = it->key;
                hash[key].ttl ++;
                if (hash[key].ttl > LINK_TIME_LIMIT) {
                        hash[key].used = HASH_OCCUP;
                        memcpy(&hash[key], 0, 2*IP_ALEN);
                        hash[key].ttl = 0;
                        pre->next = it->next;
                        free(it);
                }
                else
                        pre = pre->next;

                it = pre->next;
        }

        tick ++;
        if ((tick & 0x11) == 0)
                for (i = 0; i < lk_cnt; i ++)
                        linkers[i].load /= 2;
}

/* Load Balance according to each Linkers' Load TODO */
static const ip_ptr linker_alloc(struct iphdr* iph)
{
        int i, ln;
        int key = get_hash_key(&iph->saddr, &iph->daddr, LINK_ALLOC);

        if (key == -1) {
                fprintf(stderr, "[Overhead] Access Denied\n");
                return null;
        }

        if (hash[key].link_n == 0xff) {
                ln = 0;
                for (i = 1; i < lk_cnt; i ++)
                        if (linkers[ln].load > linkers[i].load)
                                ln = i;
        }

        linkers[ln].load += htons(iph->tot_len);
        return linkers[ln].ip_addr;
}

/* Naive way of load balance */
static const ip_ptr linker_alloc_easy(struct iphdr* iph)
{
        int *ip1, *ip2;
        int ln;

        ip1 = (int*)&iph->saddr;
        ip2 = (int*)&iph->daddr;
        ln = (*ip1 + *ip2) % lk_cnt;
        
        return linkers[ln].ip_addr;
}


static unsigned short ip_id = 0x1000;

/* It seems that the MTU in the my computer is about 1500,
 * do I need to work on the fragmentation part ?
 */
void ip_pack_forward(pac_ptr packet)
{
        struct iphdr* iph = (struct iphdr*)packet;
        const ip_ptr tip = linker_alloc_easy(iph);
        int len = htons(iph->tot_len);
        const mac_ptr mac_addr;

        if (tip == null)
                return ;

        if (len + IP_HLEN > IPPAC_MTU) {
                printf("Over MTU\n");
                ip_fragment(iph);
                return ;
        }

        memcpy(&ip_home.daddr, tip, IP_ALEN);
        ip_home.tot_len = htons(len + IP_HLEN);
        ip_home.ttl = iph->ttl;
        ip_home.id = htons(ip_id ++);
        ip_home.tos = iph->tos;
        ip_setcheck(&ip_home);

        memcpy(ip_buf, &ip_home, IP_HLEN);
        memcpy(&ip_buf[IP_HLEN], packet, len);

        ip_forward(ip_buf);
}

