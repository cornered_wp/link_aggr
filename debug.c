#include <linux/ip.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <net/if_arp.h>
#include <stdio.h>
#include <assert.h>

#include "common.h"
#include "arp.h"

static char *debug_tcp = "tcp";
static char *debug_udp = "udp";
static char *debug_vpn = "vpn";


int debug_sent_ip(pac_ptr packet)
{

        struct iphdr *ip = (struct iphdr*)packet;
        uint16 *port;
        char *proto;

        if (ip->protocol == IPPROTO_TCP)
                proto = debug_tcp;
        else if (ip->protocol == IPPROTO_UDP)
                proto = debug_udp;
        else if (ip->protocol == IP_VPN_PROTO)
                proto = debug_vpn;
        else {
                proto = "unk";
                return 1;
        }

        port = (uint16*)&packet[ip->ihl * 4];
        printf("\n    IP(%s): (%s, ", proto, ip_tostr((ip_ptr)&ip->saddr));
        printf("%s, %d)\n", ip_tostr((ip_ptr)&ip->daddr), htons(*port));

        if (ip->protocol == IP_VPN_PROTO)
                debug_sent_ip(&packet[IP_HLEN]);

        return 0;
}

int debug_sent_arp(pac_ptr packet)
{
        struct arphdr2 *arp = (struct arphdr2*)packet;

        printf("\n      ARP(");
        if (arp->ar_op == htons(ARPOP_REPLY)) {
                printf("REPLY) %s is at ", ip_tostr(arp->ar_sip));
                mac_println(arp->ar_sha);
        }
        else if (arp->ar_op == htons(ARPOP_REQUEST)) {
                printf("REQUEST) who is %s\n", ip_tostr(arp->ar_tip));
        }
}

int debug_sent_frame(pac_ptr frame, int len)
{
        struct ethhdr *eth = (struct ethhdr*)frame;

        printf("SEND: Ether Frame of Size %d:\n", len);
        printf("    MAC <");mac_print(eth->h_source);printf(", ");
        mac_print(eth->h_dest);
        printf(">  ");

        if (htons(eth->h_proto) == ETH_P_IP)
                debug_sent_ip(&frame[ETH_HLEN]);
        else if (htons(eth->h_proto) == ETH_P_ARP)
                debug_sent_arp(&frame[ETH_HLEN]);
        else
                printf("\n");
}


int debug_receive_frame(pac_ptr frame, int len)
{
        struct ethhdr *eth = (struct ethhdr*)frame;

        printf("RECV: Ether Frame of Size %d:\n", len);
        printf("    MAC <");mac_print(eth->h_source);printf(", ");
        mac_print(eth->h_dest);
        printf(">  ");

        if (htons(eth->h_proto) == ETH_P_IP)
                debug_sent_ip(&frame[ETH_HLEN]);
        else if (htons(eth->h_proto) == ETH_P_ARP)
                debug_sent_arp(&frame[ETH_HLEN]);
        else
                printf("\n");
}
