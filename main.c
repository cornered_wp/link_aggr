#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/in.h>
#include <linux/if_ether.h>

#include "common.h"


unsigned char buf[1600]; //Well, this needs more concern
int sock_fd;


void timer_init();
int linker_init();

void config()
{
        char line[100];
        FILE *fd = fopen(".config", "r");
        if (fd == NULL) {
                fprintf(stderr, "Error: can\'t open configuration file");
                exit(1);
        }

        fgets(buf, 99, fd);
        if (!mac_init(buf)) {
                fprintf(stderr, "\nError: .config MAC format wrong\n");
                exit(1);
        }

        fgets(buf, 99, fd);
        ip_init(buf);

#ifdef LINKER_DEF
        nat_init();
#else
        linker_init();
#endif

        fclose(fd);

	if ((sock_fd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
		printf("Error: can\'t create raw socket\n");
                exit(1);
        }

        timer_init();

}

void listen_pack(int sock)
{
        int n_read;

        while (true) {
                n_read = recv(sock_fd, buf, 2048, 0);

                if (n_read < 42) {
                        fprintf(stderr, "Error: Broken Frame\n");
                        continue;
                }

                mac_handle(buf, n_read);
        }
}

void main_test()
{
        arp_test();
}

int main(int argc, char* argv[])
{

        config();
        main_test();

        listen_pack(sock_fd);
        close(sock_fd);
}
