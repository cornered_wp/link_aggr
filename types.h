#ifndef TYPES_H
#define TYPES_H

#define uint32  unsigned int
#define bool    unsigned char

#define ip_ptr  unsigned char*
#define mac_ptr unsigned char*
#define pac_ptr unsigned char*

#define uint16  unsigned short

#endif
