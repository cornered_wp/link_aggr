#ifndef __CONST_H__
#define __CONST_H__

#define true            1
#define false           0

#define null            (void*)0


/* According to RTF 790, 80~254 are unassigned */
#define IP_VPN_PROTO       117


#define IP_HLEN         20
#define IP_ALEN         4


#define MAX_BUF_SIZE    1600
#define IPPAC_MTU       (MAX_BUF_SIZE - 20)


/* Any link should not be idle for more than the threshold */
#define LINK_TIME_LIMIT 1200

#endif
