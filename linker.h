#ifndef __LINKER_H__
bool nat_assign(ip_ptr saddr, ip_ptr daddr, uint16 *port);
bool nat_reverse(int* saddr, int* daddr, uint16 *port);
void tcp_checksum(pac_ptr packet);
#endif
